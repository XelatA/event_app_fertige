Geburtstag von Karl-Heinz ; Start Geburtstag;         0.00 ;         0.00 ;         0.00 ;         0.00
Geburtstag von Karl-Heinz ; Geburtstag planen;         0.00 ;         5.00 ;         0.00 ;         5.00
Geburtstag von Karl-Heinz ; Raum und Musik;         5.00 ;         8.00 ;         5.00 ;         8.00
Geburtstag von Karl-Heinz ; Catering, Torte und Florist;         5.00 ;         6.00 ;         5.00 ;         6.00
Geburtstag von Karl-Heinz ; Vorlaufzeit Geburtstag;         6.00 ;        13.00 ;         6.00 ;        13.00
Geburtstag von Karl-Heinz ; Diashow;         8.00 ;        13.00 ;         8.00 ;        13.00
Geburtstag von Karl-Heinz ; Dekoration;         8.00 ;        10.00 ;        11.00 ;        13.00
Geburtstag von Karl-Heinz ; Ende Geburtstag;        13.00 ;        13.00 ;        13.00 ;        13.00
Hochzeit von Susi und Franz ; Start Hochzeit;         0.00 ;         0.00 ;         2.00 ;         2.00
Hochzeit von Susi und Franz ; Hochzeit planen;         0.00 ;        15.00 ;         2.00 ;        17.00
Hochzeit von Susi und Franz ; Raum Hochzeit;        15.00 ;        19.00 ;        23.00 ;        27.00
Hochzeit von Susi und Franz ; Raumdekoration und Sitzordnung;        19.00 ;        24.00 ;        27.00 ;        32.00
Hochzeit von Susi und Franz ; Catering, Florist, Life-Band und Fotograf;        15.00 ;        16.00 ;        17.00 ;        18.00
Hochzeit von Susi und Franz ; 1. Vorlaufzeit Hochzeit;        16.00 ;        30.00 ;        18.00 ;        32.00
Hochzeit von Susi und Franz ; Verkostung Hochzeitstorte;        15.00 ;        16.00 ;        20.00 ;        21.00
Hochzeit von Susi und Franz ; Hochzeitstorte bestellen;        16.00 ;        17.00 ;        21.00 ;        22.00
Hochzeit von Susi und Franz ; 2. Vorlaufzeit Hochzeit;        17.00 ;        27.00 ;        22.00 ;        32.00
Hochzeit von Susi und Franz ; Ende Hochzeit;        30.00 ;        30.00 ;        32.00 ;        32.00
Firmenfeier von TUI ; Start Firmenfeier;         0.00 ;         0.00 ;         5.00 ;         5.00
Firmenfeier von TUI ; Firmenfeier planen;         0.00 ;         7.00 ;         5.00 ;        12.00
Firmenfeier von TUI ; Raum und Musik Firmenevent;         7.00 ;         8.00 ;        17.00 ;        18.00
Firmenfeier von TUI ; Raumdekoration Rednerpult;         8.00 ;        10.00 ;        18.00 ;        20.00
Firmenfeier von TUI ; Catering und Florist;         7.00 ;         8.00 ;        12.00 ;        13.00
Firmenfeier von TUI ; Vorlaufzeit Firmenfeier;         8.00 ;        15.00 ;        13.00 ;        20.00
Firmenfeier von TUI ; Ende Firmenfeier;        15.00 ;        15.00 ;        20.00 ;        20.00
