class EventJobPeriodAssociation < ActiveRecord::Base
  belongs_to :event
  belongs_to :job
  belongs_to :period
end
